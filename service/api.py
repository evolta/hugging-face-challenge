from flask import Flask
from flask import request
from transformers import pipeline

app = Flask(__name__)


@app.route('/', methods=['GET'])
def health_check():
    """Health check only route"""
    return "Hello World", 200


@app.route('/sentiment-analysis', methods=['POST'])
def return_analysis():
    """Route to receive an analysis on POST from the tweet data given by our API's user.

    The user should send a POST query to: http://<api/url>/sentiment-analysis with a body containing a JSON object.
    The json objects needs to have this format:
    ```json
    {
      "tweet": "TEXT TO ANALYSE"
    }
    ```

    Example curl command
    ```shell
    curl -v -d '{\"tweet\":\"Some text\"}' -H 'Content-Type: application/json' <The API's URL>/sentiment-analysis
    ```

    Returns
    -------
    Https response: str, Code
    """
    # Getting the text that was sent to us
    payload = request.get_json() if request.get_json() else {}
    text_to_analyse = payload.get("tweet")

    # Verify if it is a tweet (we don't want to process entire books :)
    if text_to_analyse and len(text_to_analyse) <= 280:
        # Create classifier
        classifier = pipeline('sentiment-analysis')

        # Marking our prediction and sending back the first result
        # in the list the should only be one given our input.
        out = classifier(text_to_analyse)[0]
        return out

    else:
        if not text_to_analyse:
            return 'Unauthorized: Un-parsable text.', 403
        return 'Unauthorized: Text is too long for this API to handle.', 403


if __name__ == "__main__":
    # Run app
    app.run(host="0.0.0.0")