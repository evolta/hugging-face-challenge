import api
import serverless_wsgi


def handler(event, context):
    """The handler only transforms the API incomming data into wsgi format so flask can use it."""
    return serverless_wsgi.handle_request(api.app, event, context)
