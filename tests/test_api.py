import json
import random
import string

from api import app


def test_sentiment_health_check():
    # Given
    # When
    with app.test_client() as test_client:
        response = test_client.get('/')

        # Then
        assert b'Hello World' in response.get_data()
        assert response.status_code == 200


def test_sentiment_analysis_route():
    # Given
    test_post_data = {
        "tweet": "I'm feeling great today"
    }
    expected_label = "POSITIVE"

    # When
    with app.test_client() as test_client:
        response = test_client.post(
            '/sentiment-analysis',
            data=json.dumps(test_post_data),
            content_type='application/json'
        )

        # Then
        assert response.status_code == 200
        assert response.json["label"] == expected_label


def test_sentiment_analysis_route_too_long_string():
    # Given
    test_post_data = {
        "tweet": ''.join(random.choice(string.ascii_lowercase) for i in range(300))
    }

    # When
    with app.test_client() as test_client:
        response = test_client.post(
            '/sentiment-analysis',
            data=json.dumps(test_post_data),
            content_type='application/json'
        )

        # Then
        assert response.status_code == 403
        assert b'Unauthorized: Text is too long for this API to handle.' in response.get_data()


def test_sentiment_analysis_route_wrong_data():
    # Given
    test_post_data = {
        "twit": "I'm feeling great today"
    }

    # When
    with app.test_client() as test_client:
        response = test_client.post(
            '/sentiment-analysis',
            data=json.dumps(test_post_data),
            content_type='application/json'
        )

        # Then
        assert response.status_code == 403
        assert b'Unauthorized: Un-parsable text.' in response.get_data()
