# hugging-face-challenge

- [Project code architecture](#project-code-architecture)
- [The subject](#the-subject)
    * [Part 1](#part-1)
    * [Part 2](#part-2)
    * [Part 3,4](#part-3-4)
    * [Part 5](#part-5)
- [A few notes on the CI/CD and reproducibility](#a-few-notes-on-the-ci-cd-and-reproducibility)
- [The journey](#the-journey)
- [Reflexions](#reflexions)
- [Infrastructure costs](#infrastructure-costs)
- [Access](#access)
    * [API URLs](#api-urls)
    * [Monitoring (Need to be connected via AWS SSO)](#monitoring--need-to-be-connected-via-aws-sso-)
        + [API Gateway](#api-gateway)
        + [Lambda: Metrics, X-RAY, Logs](#lambda--metrics--x-ray--logs)


For the complete execution of this exercise we assume an AWS landing zone has already been created. 
In this case we are going to use the provided landing zone (Default VPC and Subnets) created by AWS on a new account.

# Project code architecture
```
.
├── .gitlab-ci.yml  # Gitlab CI CD pipelines configuration.
├── .gitignore
├── Dockerfile  # Custom CI/CD deployment image for Serverless and docker in docker 
├── tests  # Unit test directory (pytest framework used).
│   └── test_api.py
├── service  # The API service directory and main code source
│   ├── .gitignore
│   ├── api.py  # Flask API route definitions and main code file
│   ├── app.py  # Lambda handler code and wsgi converter
│   ├── Dockerfile  # Custom Execution environment container definition
│   ├── serverless.yml  # Serverless framework deployment parameters and definition
│   └── __init__.py
├── load_tests  # Load test tools
│   ├── artillery.yml  # Artillery load test configuration
│   ├── report.json  # Report of the tests ran
│   └── loadmill  # Old directory with loadmill results and configuration
│       └── load_test_results_150_calls.csv
│       └── loadmill_load_test_flow.json
└── README.md
```


# The subject

In this part we will mainly be speaking about the technical choices made and answer directly to the subject given. 
In the next section you can find information about the journey it was to implement the whole tool.

## Part 1 

> Pick one of the Pipeline types from the famous `huggingface/transformers` open source
NLP library:
https://github.com/huggingface/transformers/tree/master/src/transformers/pipelines
A Pipeline type is a standardized abstraction for a particular text-based task, that works
with multiple models trained or fine-tuned by the huggingface community


For this challenge I'm going to use one of the example pipelines provided in the documentation:
```python
>>> from transformers import pipeline

# Allocate a pipeline for sentiment-analysis
>>> classifier = pipeline('sentiment-analysis')
>>> classifier('We are very happy to include pipeline into the transformers repository.')
[{'label': 'POSITIVE', 'score': 0.9978193640708923}]
```

> Note that in the end it is really easy to change the algorithm we use.

## Part 2 

> Describe which business use case an API for this pipeline could solve, and set up
expectations of performance for that use case.

Sentiment analysis on texts is a great use case. Applications are large one example can be to provide a media company 
with live information on the feedback people are giving to show. For this the data could be coming from tweets that are 
coming in response to an HashTag the media company has put in place for their show. The company needs the sentiment in 
real time we are assuming they are sending the text from the tweets directly from to our API and are expecting a 
response under a half minute so that they can aggregate the data of their stream and get a mean/general sentiment from the 
user feedback. The company expects to be able to get hundreds of tweets per minute.

So the requirements we need to meet are the following: 
- 1000 responses in one minute. 
- Max latency 30s.


## Part 3,4

> Implement an HTTP API on top of the chosen pipeline. Pick any language/framework
you want, but explain why.

> Deploy your API in the Cloud. Pick any cloud, any hardware. We can reimburse any
hosting costs you may incur.
Your deployment should be reproducible and somewhat scalable.

So to cope with the demand we are going to create a very simple architecture on AWS that can respond to millions of 
requests per day. So we are going to make a completely serverless architecture on top of our NLP API.

Using AWS for me is an obvious choice first of all because I know it better being certified on this cloud and because 
most of the tooling I use integrates best with it.

You can find a simple architecture diagram of the solution implemented bellow:

![Architecture](docs/architecture_diagram.svg)

The lambda responding to the API calls will be writen in Flask (because I know the framework better) 
and python is an obvious choice for the two following reasons: 
1. Most developers today know how to code python and this eases the on-boarding process but also permits to easily find 
   specialist. In effect with a higher offer pool the developers can have sometimes cheaper rates than specialists in Scala 
   for example (its a fact). 
2. The hugging face library is written in python. The in house developers know how to use this language, and it 
   drastically eases the integration process of the library in our API.
   
In order to provide a stateless and easy deployment of our API, I've decided to use the serverless framework. This 
framework is really easy to use, its open source and provides us with a stateless environment that we can easily 
reproduce on other AWS accounts.

To manage our python environment we are going to go entirely on docker containers this allows a better reproducibility 
and permits our whole environment to work into the CI/CD pipelines that are put into place. 

The choice of using CI/CD comes in really handy here it automatizes completely the unit tests and load tests created so 
no need to launch anything this is done automatically each time a commit arrives in gitlab. Using gitlab was also a 
choice because CI/CD is easy to use using the framework they provide.


## Part 5

> Load test and monitor your API’s performance: Pick any instrumentation tooling you like
(Grafana, ELK, Prometheus, anything else), then flood the endpoint with traffic
(simulated or real) and report how it’s scaling.


After some reflexion and tests I've made the choice to use artillery to do the load testing. The framework is really easy 
to use and configure, its open source, and it provides us with all the functionalities we need. 
The tool was configured to make the tests on the constraints we had and in the end the API responded well. 
(You have a report under `load_test/report.json`). 

In the end the API responded correctly with a mean response time of: 13.7s. It also was 
capable of pressing 9000 calls in 5 minutes so could handle 108000 calls in an hour. The error rate was under 1% with 
an error rate of 0,43% (39 calls). The load test was made on a warm lambda so could differ on cold start. The idea in 
the future would be too keep the production API warm if this was required.

For monitoring lets I've chosen to go the easiest way possible. AWS provides us with API gateway monitoring as well as 
lambda monitoring (using cloudwatch metrics and cloudwatch logs). To go a bit further I've also activated Xray on our 
service through the serverless framework which activates Xray in AWS and eases monitoring and debugging.

> Not if you run the load test job you are able to download the report from gitlab as it keeps the report artifact for 
> a week.

# A few notes on the CI/CD and reproducibility
To avoid having any problems with building the API in your environment or having you setting up any tools,
I've taken the liberty of providing you with the CI/CD to build and deploy the API in AWS.

All the steps can be reproduced locally with Docker and following what the CI/CD does in `.gitlab-ci.yml`. Each
portion represents a step in the deployment and test. The image names are provided but of course some of them would be
built locally (the root one, and the one in service).


The CI/CD is configured to do the following:
- All feature and develop branch make unit test image and run unit tests.
- Staging branch environment triggers deployment update on dev environment, and a simple integration test calling the
  sentiment analysis route. It also updates the deployment image to reflect last changes made on it to deploy correctly.
  You also have a manual trigerrable job to delete the dev environment.

- Master branch updates prod environment and runs loads test on the prod environment (this should be moved to a
  uat environment in the future). You also have a manual trigerrable job to delete the prod environment.
  

> Note if needs to be reproduced on another account CI/CD will need to be deployed and in the CI/CD and load tests
> all URL references need also to be updated!


# The journey

In the last past years I've learned a lot about serverless architectures and I wanted to go with that type of pattern in 
order to deploy our API. A serverless architecture has the clear advantage to scale really high at a low cost and with the correct
frameworks its actually really easy to put into place.

The journey started by putting my CI/CD runners in place no big deal on this part it was pretty straightforward 
using gitlab documentation. Right now the runner is a docker container running on a t3.micro machine on my AWS account.

Creating the API code in flask was pretty simple no big problems either. Creating the unit tests was also quite trivial 
in our case.

Then I started using zappa knowing the framework did make it the first obvious choice but in the end I had a big 
problem: tensorflow-cpu packages is actually more then 500MB large and so after numerous attempts to reduce the package
size/part it and keeping it on S3. I wasn't able to overcome this limitation on python runtime lambdas. 

I had a big problem and needed to make a change. The solution came pretty fast: use a docker runtime which allows using 
10GB docker images into our lambdas. Another issue came zappa doesn't handle this type of deployment. So I started using
the serverless framework instead. Serverless is quite easy to use so once I converted everything and created my runtime 
image and my ci/cd custom image all was good to deploy and things went pretty smoothly from there.

# Reflexions

The system is quite reactive but the cold start is an issue creating API gateway timeouts because the 30sec+ response 
time. To diminish this response time maybe creating a pretrained tflite model could be an interesting way to go.

> Note: Maybe installing the tensorflow package via conda is a solution for performance I saw some article speaking about that.

In terms of scaling I noticed that going to higher rates increases the error rate. So a solution to scale massively 
could be to make use of route53 to distribute the load to a fleet of the same APIs deployed in different AWS regions 
(or accounts). Given the fact that all calls should be different caching the responses in cloudfront doesn't have any impact other than
increasing costs.

We could also make asynchronous responses instead having the data producer just send the data receiving a request id
and pulling the result a few seconds later. Or we could publish the result on an SNS topic and then the producer can 
subscribe to the topic.

Changing this repository to use another model is quite fast and so it is probably possible to create a simple 
tool out of it to easily deploy transformers pipeline APIs.

> Note: security needs to be put in place currently anybody from everywhere can access the API.

# Infrastructure costs

In total the price of the multiple tests (including a few load tests) is pretty low as you can see in the picture bellow:

![Price Overview](docs/costs.png)

> Cost seem low for the moment, we haven't attained the free tier services limits. 


# Access

## API URLs
* DEV: https://5qzjt141zk.execute-api.eu-west-1.amazonaws.com/dev/
* PROD: https://0o8phe1heg.execute-api.eu-west-1.amazonaws.com/prod/

Routes:
* / -> health check
* /sentiment-analysis -> Analyse your tweet text payload:
```json
{
  "tweet": "<YOUR TEXT HERE>"
}
```


## Monitoring (Need to be connected via AWS SSO)
### API Gateway

* DEV: https://eu-west-1.console.aws.amazon.com/apigateway/home?region=eu-west-1#/apis/5qzjt141zk/dashboard
* PROD: https://eu-west-1.console.aws.amazon.com/apigateway/home?region=eu-west-1#/apis/0o8phe1heg/dashboard

> Metrics only

### Lambda: Metrics, X-RAY, Logs
On the lambda monitoring page under you have multiple monitoring features including x-ray tracing, metrics and logs.

* DEV: https://eu-west-1.console.aws.amazon.com/lambda/home?region=eu-west-1#/functions/sentiment-analysis-dev-sentiment?tab=monitoring
* PROD: https://eu-west-1.console.aws.amazon.com/lambda/home?region=eu-west-1#/functions/sentiment-analysis-prod-sentiment?tab=monitoring

> X-Ray enables monitoring on both the lambdas and the API gateway so you can get a sense of the health of the whole system.